package com.company;

public class PlayerException extends Exception {
    public PlayerException() {
        super("No player.");
    }
}