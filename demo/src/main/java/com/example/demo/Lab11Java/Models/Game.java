package com.company;

import java.util.List;

public class Game {
    List<Player> playerList;

    public Game(List<Player>list) {
        this.playerList=list;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }
}